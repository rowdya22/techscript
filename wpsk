#!/bin/bash

# Title: Wordpress Swiss Knife
# Author: Chad D.
# Last Edited: 07.14.15
# =====================================================================================

# Constants
# wpFiles=(index.php license.txt readme.html wp-activate.php wp-admin wp-blog-header.php wp-comments-post.php wp-config.php wp-content wp-cron.php wp-includes wp-links-opml.php wp-load.php wp-login.php wp-mail.php wp-settings.php wp-signup.php wp-trackback.php xmlrpc.php)

wpFiles=(.htaccess index.php license.txt php.ini readme.html wp-* xmlrpc.php)
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'

function wptotal() {
	case "$1" in
	(-h)	printf "\nThis will list details of all wordpress sites present on the account\n\n"
		return
		;;
	
	('')	# Find path to wpconfig files and place in an array
		wpLocation=($(find ~/public_html -maxdepth 5 -type f -name 'wp-config.php'))
	
		# Output total amount of wp installs
		printf "\nThere is a total of %s wordpress install(s)\n" "${#wpLocation[@]}"

		# Output details of each wordpress insall using wpdetails function
		for ((i = 0; i < ${#wpLocation[@]}; i++))
		do
			printf "\n($(($i+1)))" # number for each install
			wpdetails ${wpLocation[$i]}
		done
		;;
	esac
}

function wpdetails() {
	case "$1" in
	(-h)	printf "\nThis will list details of the wordpress in the present directory\n\n"
		return
		;;
		
	('')	# Confirm local wp-config.php is present, if not present, indicate so 
	  	if [ -f  wp-config.php ]; then
	  		set -- "wp-config.php"
	  	else
	  		printf "\nThere is no wp-config.php file present\n\n" && return 1
	  	fi  
	  	;;
	esac
	
	# Assign Database variables
	read dbHost dbName dbPass dbUser dbPref <<< $(cat $1 | egrep "^[^/].*'DB_(NAME|USER|PASSWORD|HOST[^_])|table_prefix" | sort -d | sed "s/.*[\"']\(.*\)[\"'].*;.*/\1/")
	
	# Determine wordpress location
	if [ $1 == "wp-config.php" ]; then
		local loc="Current Working Directory"	
	else
		local loc=${1%wp-config.php*}
	fi

	# Access database and get Site URL and Home URL
	local siteURL=$(mysql -u $dbUser -p$dbPass $dbName -se "SELECT option_value FROM "$dbPref"options WHERE option_name = 'siteurl';")
	local homeURL=$(mysql -u $dbUser -p$dbPass $dbName -se "SELECT option_value FROM "$dbPref"options WHERE option_name = 'home';")
	
	# If database read error indicate such
	if [ -z "$siteURL" ]; then
		local siteURL='Database read error...'
	fi
	
	if [ -z "$homeURL" ]; then
		local homeURL='Database read error...'
	fi
	
	# Output details
	printf "\tLocation:\t%s\n\tSite Url:\t%s\n\tHome Url:\t%s\n\tDatabase:\t%s\n\tUser:\t\t%s\n\tPassword:\t%s\n\tPrefix:\t\t%s\n" "~/public_html${loc##/home*public_html}" "$siteURL" "$homeURL" "$dbName" "$dbUser" "$dbPass" "$dbPref"
}

function wpbackup() {
	case "$1" in
	(-h) 	printf "\nThis will result in the backing up of the wordpress site present in the working directory\n\n"
		return 1
		;;
			
	(-t)	printf "\nWhich wordpress install would you like to backup?\n"
		wptotal # Call to wptotal function
		
		# Loop until user selection is valid
		while :
		do
			printf "\nSelection: "; read selectWP
			
			if [ "$selectWP" -gt "0" ] && [ "$selectWP" -le "${#wpLocation[@]}" ]; then
				break		
			fi
				
			printf "\nSorry, %s is not a valid response. Try again.\n" "$selectWP"
		done
			
		# Decrement $selectWP and access wpLocation array, assign $loc to path of wordpress selected
		local loc=${wpLocation[$(($selectWP - 1))]%wp-config.php*}
		;;
			
	('')	local loc=$(pwd)/
		;;
	esac

	if [ -f "$loc"wp-config.php ]; then
		# Create backup directory, and sync wordpress files
		local newDir="$loc"backup_wordpress_$(date +"%m_%d_%y")
		mkdir $newDir
		printf "\nBackup in progress...\n"
		for ((i = 0; i < ${#wpFiles[@]}; i++))
		do
			rsync -auvxq "$loc"${wpFiles[$i]} $newDir && printf "%40sSuccess\r\t${wpFiles[$i]}\n" || printf "%40sError\r\t${wpFiles[$i]}\n"		
		done
		
		# Assign Database variables
		read dbHost dbName dbPass dbUser dbPref <<< $(cat "$loc"wp-config.php | egrep "^[^/].*'DB_(NAME|USER|PASSWORD|HOST[^_])|table_prefix" | sort -d | sed "s/.*[\"']\(.*\)[\"'].*;.*/\1/")
		
		# Extract Database
		mysqldump -u "$dbUser" -p"$dbPass" "$dbName" > "$newDir/$dbName".sql && printf "%40s${blue}Success${end}\r\t$dbName database\n" || printf "%40s${red}Error${end}\r\t$dbName database\n"
		
		printf "\nBackup complete\n\nLocation: %s\n" "$loc"
		
		printf "\nWould you like to compress the backup?\n\n(1)\tzip\n(2)\ttar.gz\n(3)\tDo not compress\n"
		
		#Loop until user selection is valid
		while :
		do
			printf "\nSelection: "; read selCompress
	
			if [ "$selCompress" -gt "0" ] && [ "$selCompress" -le "3" ]; then
				break		
			fi
				
			printf "\nSorry, %s is not a valid response. Try again.\n" "$selCompress"
		done
		
		case "$selCompress" in
		(1)	zip -r $newDir.zip $newDir && printf "\nCompression complete\n\n" || printf "\nCompression failed\n\n"
			rm -fr $newDir		
			;;
		(2)	tar cvzf $newDir.tar.gz $newDir && printf "\nCompression complete\n\n" || printf "\nCompression failed\n\n"
			rm -fr $newDir
			;;
		(3)	printf "\nNo Compression was done\n\n"
			;;
		esac		
	else
		printf "\nThere is no wp-config.php file present\n\n" && return 1
	fi
}

function wpmigrate() {
	case "$1" in
	(-h) 	printf "\nThis will result in migrating of the wordpress site present in the working directory\n"
		;;
			
	(-t)	printf "\nWhich wordpress install would you like to migrate?\n"
		wptotal # Call to wptotal function
			
		# Loop until user selection is valid
		while :
		do
			printf "\nSelection: "; read selectWP
	
			if [ "$selectWP" -gt "0" ] && [ "$selectWP" -le "${#wpLocation[@]}" ]; then
				break		
			fi
				
			printf "\nSorry, %s is not a valid response. Try again.\n" "$selectWP"
		done
			
		# Decrement $selectWP and access $wpLocation array, assign $loc to path of wordpress selected
		local loc=${wpLocation[$(($selectWP - 1))]%wp-config.php*}
		;;
			
	('')	local loc=$(pwd)/
		;;
	esac
	
	if [ -f "$loc"wp-config.php ]; then
		local migrateDir=~/public_html
		local migrate=false
		
		printf "\nWhere would you like to migrate to?\n"
		
		# Loop using availdir function, until user selects directory they want to migrate to
		until [ $migrate == true ];
		do		
			printf "\nAvailable directories in %s\n\n" "~/public_html${migrateDir##/home*public_html}"
			availdir $migrateDir	
		done
			
		while :
		do
			printf "\nWould you like to create a sub-directory within %s?\n" "~/public_html${migrateDir##/home*public_html}"
			printf "\n(1)\tYes\n(2)\tNo\n"
			printf "\nSelection: "; read selectSub
			
			if [ "$selectSub" -eq 1 ]; then
				printf "\nWhat would you like to name the sub-directory?\n\nSub-directory name: "; read subDir
				printf "\nYou would like to:\n\n\tCreate sub-directory called: %s\n\tMigrate: %s\n\tTo: %s\n\n" "$subDir" "~/public_html${loc##/home*public_html}" "~/public_html${migrateDir##/home*public_html}$subDir"
				printf "Correct?""\n\n(1)\tYes\n(2)\tNo\n"
				printf "\nSelection: "; read selectMig
				printf "\n"
				
				if [ "$selectMig" -eq 1 ]; then
					migrateDir=$migrateDir$subDir
					mkdir $migrateDir && printf "%40sSuccess\r\tDirectory creation\n" || printf "%40sError\r\tDirectory creation\n"
					break
					
				elif [ "$selectMig" -eq 2 ]; then
					printf "\nMigration cancelled\n\n"
					return 1
				else
					printf "\nSorry, %s is not a valid response. Try again.\n" "$selectSub"
				fi			
							
			elif [ "$selectSub" -eq 2 ]; then
				break
			else
				printf "\nSorry, %s is not a valid response. Try again.\n" "$selectSub"
			fi						
		done
		
		for ((i = 0; i < ${#wpFiles[@]}; i++))
		do
			mv -f "$loc"${wpFiles[$i]} $migrateDir && printf "%40s${blue}Success${end}\r\t${wpFiles[$i]}\n" || printf "%40s${red}Error${end}\r\t${wpFiles[$i]}\n"		
		done
		
		printf "\nWordpress migration task complete\n"	
	else
		printf "\nThere is no wp-config.php file present\n\n" && return 1
	fi
}

function availdir() {
	local availDir=($1/*/) # Create array containing path to directories of given directory

	for ((i = 0; i < ${#availDir[@]}; i++))
	do
		local dir=${availDir[$i]} # Print out each directory for user to select from
		printf "($(($i + 1)))\t%s\n" "${dir##$1/}"
	done
	
	local parentDir="${1%/*/}" # Create variable to reference parent directory, print out parent directory and migrate option
	printf "\n($((${#availDir[@]} + 1)))\tBack: %s\n" "~/public_html${parentDir##/home*public_html}"
	printf "($((${#availDir[@]} + 2)))\tMigrate to: %s\n" "~/public_html${1##/home*public_html}"
	
	# Read users selection and handle accordingly. View contents of selected directory, move up, or migrate to selected directory
	while :
	do
		printf "\nSelection: "; read selectDir
		
		if [ "$selectDir" -gt 0 ] && [ "$selectDir" -le "${#availDir[@]}" ]; then 
			migrateDir=${availDir[$(($selectDir - 1))]}
			echo "$migrateDir"
			break
		elif [ "$selectDir" -eq "$((${#availDir[@]} + 1))" ]; then
			migrateDir=${1%/*/}
			echo "$migrateDir"
			break
		elif [ "$selectDir" -eq "$((${#availDir[@]} + 2))" ]; then
			migrate=true
			echo "$migrateDir"
			break
		fi	
		
		printf "\nSorry, %s is not a valid response. Try again.\n" "$selectDir"
	done
}

wpnew() {
	case "$1" in
	(-h)	printf "\nThis will download the lastest version of wordpress\n\n"
		return
		;;
	(-p)	printf "\nWhere would you like to install wordpress?\n"
		
		;;
	('')	local pwd=`pwd`
		;;
	esac
	
	local present=false
	local quit=false
	
	# check if wordpress files already present, if so present=true
	for ((i = 0; i < ${#wpFiles[@]}; i++))
	do
		if [ -f "$pwd/${wpFiles[$i]}" ] || [ -d "$pwd/${wpFiles[$i]}" ]; then
			local present=true
			break
		fi
	done
	
	# if wordpress files present, print out options
	if [ $present == true ]; then
		printf "\nWordpress files present in %s\n\nExisting Wordpress files...\n" "~/public_html${pwd##/home*public_html}"
		printf "%35sInstall ${grn}new${end} wordpress\r(1)\t${red}Delete${end} existing\n"
		printf "%35sInstall ${grn}new${end} wordpress\r(2)\t${blu}Backup${end} existing\n"
		printf "(3)\tReplace core wordpress\n"
		printf "(4)\tQuit" 
			
		while :
		do
			printf "\n\nSelection: "; read rQuit
			
			case "$rQuit" in
			(1)	for ((i = 0; i < ${#wpFiles[@]}; i++))
				do
					rm -fr "$pwd/${wpFiles[$i]}" 
				done
				break
				;;
			(2|3)	local now="$pwd/`date +%l%M`_old_wordpress_`date +%m_%d_%y`"
				mkdir $now
				for ((i = 0; i < ${#wpFiles[@]}; i++))
				do
					mv -f "$pwd/${wpFiles[$i]}" $now 
				done
				
				if [ $rQuit -eq 3 ]; then
					mv $now/wp-content $pwd
					mv $now/wp-config.php $pwd
					rm -fr $now
				fi
				break
				;;

			(4)	quit=true
				break
				;;			
			esac
			
			printf "\nSorry, %s is not a valid response. Try again." "$rQuit"
		done
	fi
	
	if [ $quit == false ]; then
		temp=`mktemp -d` # Create temp directory for wordpress download
	
		# Get latest wordpress install and download to $temp, then unzip
		wget -P $temp/ https://wordpress.org/latest.zip 
		unzip $temp/latest.zip -d $temp/ 
		mv -n $temp/wordpress/* $pwd 
		rm -fr $temp
	fi
}



wperror() {
	printf "\n\nTesting yo!\n\n"
}

function wpsk() {

		echo "
           \\\        //   ///// 
            \\\  /\  //   //  //
             \\\\//\\\//   /////
              \\\  \\\   //
 	 (Migrate/Backup/Cleanup)
 	  		Swiss Knife"
 	  
 	  printf "\nA wordpress tool to help simplify the tedious things in life\n\n"
 	  printf "%20sList all wordpress installs and crucial\r\twptotal:\n%20sdetails\n"
 	  printf "\n%20sList crucial details about wordpress in\r\twpdetails:\n%20sworking directory\n"
 	  printf "\n%20sCreate backup of wordpress site\r\twpbackup:\n"
 	  printf "\n%20sMigrate wordpress site to another\r\twpmigrate:\n%20sdirectory on the account\n\n"
	  printf "\n%20sInstall latest wordpress files\r\twpnew:\n"
}

# Default welcome message
printf "\n%10s${red}Wordpress Swiss Knife${end} is fully operational...\n%10sType 'wpsk' for more details\n\n"

