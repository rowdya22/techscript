#!/bin/bash

### BEGIN ADJUSTMENTS ###
# Added echo at start of launch
# General grammar changes
# Commented out exits
### END ADJUSTMENTS ###

echo "Starting checks...."

# We set the default command for wp here
wp="wp"

# Some shared servers have to have modified wp-cli commands
# you can specify it here:
#wp=

# determine if we have a wsod.
# also, pass --skip-plugins and/or --skip-themes to skip those
function wordpress_loads() {
  [ "$($wp eval 'echo "!@";' $@)" == "!@" ]
}

function get_wordpress_creds() {
   # read in vars from wp-config.php
   read -r dbhost dbname dbpass dbuser dbprefix <<< $(cat wp-config.php | egrep "^[^/].*[\"']DB_(NAME|USER|PASSWORD|HOST[^_])|table_prefix" | sort -d | sed "s/.*[\"']\(.*\)[\"'].*;.*/\1/" )
}

# add things to the report var, to display at the end
report=""
function report_add() {
  report="${report}$@\n"
  echo $@
}

# clear cache function
function _clear_caches() {
  for each in wp-content/cache \
               wp-content/endurance ; do
    if [ -d "$each" ] ; then
      # remove cache files, excluding .htaccess and index ones
      find "$each" -type f ! -iname ".htaccess" ! -iname "index.php" ! -iname "index.html" -delete
    fi
  done 
}

#### basic sanity checks ####

if ! [ -f wp-config.php ] ; then
  echo "There is no wp-config.php file. Exiting."
  #exit 1
fi


 # check database connection
conn="$($wp eval 'echo "!@";' --skip-plugins --skip-themes | grep "Error establishing a database connection")"
if [ -n "$conn" ] ; then
  echo $conn
  # if this is the issue, there's not much we can do
  # let them know, and show the creds:
  get_wordpress_creds
  cat<<EOF
  You must fix this for the site to work.
  I'd recommend looking in the MySQL Databases on the cPanel,
  and verifying that the user has access to the database.
  Also try resetting the password for that database user
  to match what is in the wp-config.php file:
  Database host: $dbhost
  Dababase name: $dbname
  Database username: $dbuser 
  Database password: $dbpass
EOF
  # and for those who like to setup their databases to go to other servers and then blame us:
  if [ $dbhost != "localhost" ] || [ $dbhost != "127.0.0.1" ] \
  || [ "$dbhost" != "$(hostname)" ] ; then
    echo "Also, the database host may not be pointing here. It's going to $dbhost"
  fi
  #report_add "$conn"
  #exit 1
fi



# Verify there is not a index.html page
if [ -f index.html ] ; then
  report_add "WARNING: There is an index.html page that may be loading instead of wordpress"
fi



# get core version an locale
core_version=$($wp core version --skip-plugins --skip-themes)
# check that there's a core locale set, otherwise don't use it
check_core_locale="$($wp core version --extra --skip-plugins --skip-themes | grep 'Package language' | awk '{print $3}')"
[ -n "$check_core_locale" ] && core_locale="--locale=$check_core_locale"

# Verify core files are installed correctly
core_errors="$($wp core verify-checksums --version=$core_version $core_locale --skip-plugins --skip-themes 2>&1 | grep 'File doesn' | sed 's/Warning: //g')"
if [ -n "$core_errors" ] ; then
  $wp core download --skip-themes --skip-plugins --force --version="$core_version" $core_locale
  report_add $core_errors
  report_add "Reinstalled core files due to some core files missing or corrupt"
fi



# is the theme installed? our current test (echo "!@";) does not detect this
if [ -z "$($wp theme list --skip-plugins --skip-themes --format=csv --field=name --status=active)" ] ; then
  echo "The active theme is not installed. Installing it...."
  # get the theme name. afaik wpcli can't do this if the theme files are deleted.
  get_wordpress_creds
  # get stylesheet and template
  read -r stylesheet template <<< $(mysql --skip-column-names --batch -u "$dbuser" -p"$dbpass" "$dbname" -h "$dbhost" -e 'SELECT option_id, option_name, option_value FROM '$dbprefix'options WHERE option_name = "stylesheet" OR option_name = "template";' 2>&1| grep -v "Using a password on the command line interface can be insecure" | awk '{print $3}')
  if [ -z "$stylesheet" ] || [ -z "$template" ] ; then
    # we didn't get the theme
    echo "Failed to get the theme name"
    report_add "Failed to get the theme. You will need to manually install it."
  else
    # reinstall the theme
    if [ "$stylesheet" == "$template" ] ; then
      $wp theme install $stylesheet --force
    else
      $wp theme install $stylesheet --force
      $wp theme install $template --force
    fi
    # check that it worked
    if [ -n "$($wp theme list --skip-plugins --skip-themes --format=csv --field=name --status=active)" ] ; then
      report_add "Reinstalled theme $stylesheet"
    else
      report_add "Failed to reinstalled theme $stylesheet or there was another issue."
    fi
  fi
fi

# clear the cache files
_clear_caches

# did sanity check fix it?
if [ -n "$report" ] ; then
 if wordpress_loads ; then
  # the wsod should be resolved
  # echo what the problem was
  echo "The wsod was fixed"
  echo "Things that we did:"
  echo -en $report
  #exit
 fi
fi

#### end basic sanity checks ####



# is there is a wsod?
if ! wordpress_loads ; then

  
  if wordpress_loads --skip-plugins --skip-themes ; then
    
    # Run through the plugins and find ones that are problematic.
    plugin_list="$(echo $($wp plugin list --skip-plugins --skip-themes --format=csv --field=name --status=active))"
    broken_plugin=""
    #broken_plugin_list=""
    for PLUGIN in $plugin_list ; do
      
      # we will make a list of the plugins, except the one we are working on,
      # and with that we will test to see if it works with only this plugin.
      all_plugins_except_checking="$(echo $plugin_list | sed 's/'$PLUGIN'//' | sed 's/^  *//' | sed 's/  *$//' | sed 's/  */,/g' )"
      
      if ! wordpress_loads --skip-plugins=$all_plugins_except_checking --skip-themes ; then
        
        # try reinstalling it, and see if that fixes it
        plugin_version="$($wp plugin list --skip-plugins --skip-themes --name=$PLUGIN --format=csv --field=version)"
        $wp plugin install --force $PLUGIN --version=$plugin_version --skip-plugins --skip-themes
        
        # check if it's now loading
        if wordpress_loads --skip-themes ; then
          # todo: show the actual plugin name, not the path
          report_add "Reinstalling $PLUGIN fixed it."
          break
        fi
        
        # if not, log it for asking the user later
        broken_plugin="$PLUGIN ${broken_plugin}"
        #broken_plugin_list="${broken_plugin_list} --skip-plugins=$PLUGIN"
        
        ### we don't need to break anymore, as we are using --skip-plugins=plugin 
        ### to selectively not load plugins, while not actually deactivating them
        # check if the site is good. or we will list all the rest of the plugins
        #$wp plugin --skip-plugins deactivate $PLUGIN
        #if [ $(wordpress_loads --skip-themes) ] ; then
        #  break
        #fi
      fi
    
    done
    
    # if there were multiple plugins, ask which one
    if [ -n "$(echo $broken_plugin | grep " ")" ] ; then
      echo "It appears that a plugin is causing the site not to load."
      echo "Select which plugin to deactivate. Or 'q' to contunue." 
      select selected_plugin in ${broken_plugin};
      do
        if [ "$selected_plugin" == "done" ] || [ "$selected_plugin" == "" ]; then
          break
        fi
        if [ -n "$selected_plugin" ] ; then
          report_add "Deactivated $selected_plugin"
          $wp plugin --skip-plugins --skip-themes deactivate $selected_plugin
        fi
      done
    
    elif [ -n "$broken_plugin" ] ; then
      # otherwise, just deactivate the one
      report_add "Deactivated $broken_plugin"
      $wp plugin --skip-plugins --skip-themes deactivate $broken_plugin
    fi
    
    # update mojo if needed
    $wp plugin update mojo-marketplace-wp-plugin
    
  fi
  
  
  # the problem may lie with the theme but we should confirm
  # and check if we fixed it above with the plugins
  if ! wordpress_loads ; then
    
    if wordpress_loads --skip-themes ; then
      
      # what theme is active?
      active_theme="$($wp theme status --skip-themes --skip-plugins | grep '^\s*A' | awk '{print $2}')"
      theme_version="$($wp theme status --skip-themes --skip-plugins | grep '^\s*A' | awk '{print $3}')"
      
      # check that the active_theme was obtained correctly
      if [ -n "$active_theme" ] && [ -n "$theme_version" ] ; then 
        
        # is it installed. If not, reinstall it
        $wp theme is-installed $active_theme --skip-themes --skip-plugins 
        [ $? -ne 0 ] && $wp theme install $active_theme --force --skip-themes --skip-plugins --version=$theme_version
        
        echo "Would you like to try reinstalling the php file of current theme $active_theme? [N/y]"
        echo "Any edits to the theme's php files will be overwritten."
        read RESP
        if [ "$RESP" == "y" ] ; then
        
        # try to reinstall the active theme (we can't do this if the theme is not from wp.org)
        # backup the current theme
        backup_time="$(date +%F_%H-%I-%M).bak"
        theme_dir="wp-content/themes/$active_theme"
        theme_backup="wp-content/themes/${active_theme}_backup_${backup_time}.zip"
        echo "Making zip backup of current theme"
        zip -r "$theme_backup" "$theme_dir"
        report_add "Made a backup of the theme into $theme_backup"
        
        # copy the functions file out to a backup
        # remember that wordpress will delete the theme folder, so make sure it's out of there
        function_real="$theme_dir/functions.php"
        function_tmp="wp-content/themes/functions_${backup_time}.php"
        function_backup_keep="$theme_dir/functions_${backup_time}.php"
        [ -f "$function_real" ] && cp "$function_real" "$function_tmp"
        # do the reinstall
        $wp theme install $active_theme --force --skip-themes --skip-plugins --version=$theme_version
        # move the functions.php back into the dir (but not into place)
        if [ -f "$function_tmp" ] ; then
          # TODO: check if they are the same with diff
          mv "$function_tmp" "$function_backup_keep"
          report_add "I moved the theme's functions.php file to $function_backup_keep"
        fi
        #log it
        report_add "Reinstalled theme $active_theme"
        
        fi # end theme reinstall
      
      fi
      
      # check it
      if ! wordpress_loads ; then
        
        # ask user and switch to twentysixteen
        # could just do: wp theme install twentysixteen --activate
        #$wp theme is-installed twentysixteen
        #if [ $? -eq 1 ] ; then
        #  # not installed
        #  $wp theme install twentysixteen
        #fi
        echo "The current issue is that the theme is not loading."
        read -p "Would you like to switch back to the basic twentysixteen theme? [N/y]" PROMPT
        if [ "$PROMPT" == "y" ] ; then
          echo "Setting theme to twentysixteen"
          $wp theme install twentysixteen  --activate --skip-themes --skip-plugins
          report_add "Changed theme to twentysixteen"
        fi
      fi
      
    else
      report_add "Reinstalled theme."
      
    fi
    # seems there is a core problem. reinstall core files
    if ! wordpress_loads ; then
    
      $wp core download --skip-themes --skip-plugins --force --version="$core_version" $core_locale
      report_add "Reinstalled core files"
    
    fi
  fi
  
  # remove cache if needed
  if wordpress_loads ; then
    clear_caches
  fi
    
  if wordpress_loads ; then
    # the wsod should be resolved
    # echo what the problem was
    echo "The wsod was fixed"
  else
    echo "The wsod was NOT fixed."
  fi
  if [ -n "$report" ] ; then
    echo "Things that we did:"
    echo -en $report
  else
    echo "We did not do anything"
  fi
else
  
  # no wsod
  echo "Site looks good."
  
fi
